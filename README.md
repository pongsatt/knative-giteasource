# Warning!! This is a work in progress

# Gitea Event Source for Knative

Gitea Source example shows how to wire Gitea events for consumption
by a Knative Service.

> **NOTE:** It is recommended that you clone this repository in order to apply the necessary files with custom values. You can optionally also create individual files for each step that requires it.

## Deploy the Gitea source controller

You will need to:

* Create a new API by creating a Custom Resource Definiton (CRD) object that defines `GiteaSource`
* Deploy the controller which watches `GiteaSource` objects and creates a Gitea webhook.

> **NOTE:** The Kubernetes `cluster-admin` role is required to perform these steps.

1. Create the new API definition:

    ```shell
    kubectl apply -f https://gitlab.com/pongsatt/knative-giteasource/raw/master/config/crds/sources_v1alpha1_giteasource.yaml
    ```

2. Launch the controller and set all the required objects:

    ```shell
    kubectl apply -f https://gitlab.com/pongsatt/knative-giteasource/raw/master/release.yml
    ```

    If the deployment goes well you should see the following output:

    ```
    namespace "giteasource-system" created
    clusterrole.rbac.authorization.k8s.io "giteasource-manager-role" created
    clusterrole.rbac.authorization.k8s.io "giteasource-proxy-role" created
    clusterrolebinding.rbac.authorization.k8s.io "giteasource-manager-rolebinding" created
    clusterrolebinding.rbac.authorization.k8s.io "giteasource-proxy-rolebinding" created
    secret "giteasource-webhook-server-secret" created
    service "giteasource-controller-manager-metrics-service" created
    service "giteasource-controller-manager-service" created
    statefulset.apps "giteasource-controller-manager" created
    ```

    At this point you have installed the Gitea Eventing source in your Knative cluster.

3. Check that the manager is running:

    ```shell
    kubectl get pods -n giteasource-system
    ```

    Output:

    ```shell
    NAME                                READY     STATUS    RESTARTS   AGE
    giteasource-controller-manager-0   2/2       Running   0          27s
    ```

    With the controller running you can now move on to a user persona and setup a Gitea webhook as well as a function that will consume Gitea events.

## Using the Gitea Event Source

You are now ready to use the Event Source and trigger functions based on Gitea projects events.

We will:

* Create a Knative service which will receive the events. To keep things simple this service will simply dump the events to `stdout`, this is the so-called: _message_dumper_
* Create a Gitea access token and a random secret token used to secure the webhooks.
* Create the event source by posting a Gitea source object manifest to Kubernetes

### Create a Knative Service

Create a simple Knative `service` that dumps incoming messages to its log. The `service` .yaml file
defines this basic service which will receive the configured Gitea event from the GiteaSource object. 
The contents of the `service` .yaml file are as follows:

```yaml
apiVersion: serving.knative.dev/v1alpha1
kind: Service
metadata:
  name: gitea-message-dumper
spec:
  runLatest:
    configuration:
      revisionTemplate:
        spec:
          container:
            image: gcr.io/knative-releases/github.com/knative/eventing-sources/cmd/message_dumper
```

Enter the following command to create the service from `service.yaml`:

```shell
kubectl -n default apply -f https://gitlab.com/pongsatt/knative-giteasource/raw/master/message-dumper.yaml
```

### Create Gitea Tokens

1. Create a [personal access token](https://docs.gitea.com/ee/user/profile/personal_access_tokens.html)
which the Gitea source will use to register webhooks with the Gitea API. 
Also decide on a secret token that your code will use to authenticate the
incoming webhooks from Gitea ([_secretToken_](https://docs.gitea.com/ee/user/project/integrations/webhooks.html#secret-token)).

    Gitea webhooks can be created and configured with the [Hook API](https://docs.gitea.com/ee/api/projects.html#hooks)

    Here's an example for a token named "knative-test" with the
    recommended scopes:

    ![Gitea UI](personal_access_token.png "Gitea personal access token screenshot")

2. Create a file called `giteasecret.yaml` with the following values:

    ```yaml
    apiVersion: v1
    kind: Secret
    metadata:
      name: giteasecret
    type: Opaque
    stringData:
      accessToken: personal_access_token_value
      secretToken: asdfasfdsaf
    ```

    Where `accessToken` is the personal access token created in step 1. and `secretToken` (`asdfasdfas` above) is any token of your choosing.
    
    Hint: you can generate a random _secretToken_ with:

    ```shell
    head -c 8 /dev/urandom | base64
    ```

3. Apply the giteasecret using `kubectl`.

    ```shell
    kubectl --n default apply -f giteasecret.yaml
    ```

### Create Event Source for Gitea Events

1. In order to receive Gitea events, you have to create a concrete Event
Source for a specific namespace. Replace the `projectUrl` value in the file `giteaeventbinding.yaml`
  with your Gitea username and project name. For example, if your repo URL is 
  `https://gitea.com/knative-examples/functions` then use it as the value for `projectUrl`.

    ```yaml
    apiVersion: sources.eventing.pongzt.dev/v1alpha1
    kind: GiteaSource
    metadata:
      name: giteasample
    spec:
      eventTypes:
        - push
        - pull_request
        - issue_comment
      projectUrl: https://<GITEA_URL>/<USERSPACE>/<PROJECT NAME>
      accessToken:
        secretKeyRef:
          name: giteasecret
          key: accessToken
      secretToken:
        secretKeyRef:
          name: giteasecret
          key: secretToken
      sink:
        apiVersion: serving.knative.dev/v1alpha1
        kind: Service
        name: gitea-message-dumper
    ```

2. Apply the yaml file using `kubectl`:

    2.1. If you cloned this repository use your specific URL where `userspace` below points to your repository:

    ```shell
    kubectl -n default apply -f https://gitea.com/<userspace>/giteasource/raw/master/giteaeventbinding.yaml
    ```

    2.2. If you rather not clone the repo, create a file called `giteaeventbinding.yaml` with the contents above and apply it:

    ```shell
    kubectl -n default apply -f giteaeventbinding.yaml
    ```

### Verify

Verify the Gitea webhook was created by looking at the list of
webhooks under **Settings >> Integrations** in your Gitea project. A hook
should be listed that points to your Knative cluster.

Create a push event and check the logs of the Pod backing the `message-dumper`. You will see the Gitea event.

