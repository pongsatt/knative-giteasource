module gitlab.com/pongsatt/knative-giteasource

go 1.12

replace gopkg.in/go-playground/webhooks.v5 => github.com/pongsatt/webhooks v0.0.0-20190609032306-6b442b35ed67

require (
	code.gitea.io/gitea v1.9.0-dev.0.20190511102134-34eee25bd42d
	code.gitea.io/sdk/gitea v0.0.0-20190602153954-7e711e06b588
	github.com/evanphx/json-patch v4.1.0+incompatible // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-logr/logr v0.1.0 // indirect
	github.com/go-logr/zapr v0.1.0 // indirect
	github.com/go-yaml/yaml v2.1.0+incompatible // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/golang/groupcache v0.0.0-20181024230925-c65c006176ff // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/google/gofuzz v0.0.0-20170612174753-24818f796faf // indirect
	github.com/google/pprof v0.0.0-20190515194954-54271f7e092f // indirect
	github.com/google/uuid v1.1.0
	github.com/googleapis/gnostic v0.2.0 // indirect
	github.com/gregjones/httpcache v0.0.0-20181110185634-c63ab54fda8f // indirect
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	github.com/imdario/mergo v0.3.6 // indirect
	github.com/json-iterator/go v0.0.0-20180612202835-f2b4162afba3 // indirect
	github.com/knative/build v0.2.0 // indirect
	github.com/knative/pkg v0.0.0-20181126210222-862aa6dbe7b6
	github.com/knative/serving v0.2.3
	github.com/mattbaird/jsonpatch v0.0.0-20171005235357-81af80346b1a // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742 // indirect
	github.com/onsi/gomega v1.4.3
	github.com/pborman/uuid v0.0.0-20180906182336-adf5a7427709 // indirect
	github.com/peterbourgon/diskv v2.0.1+incompatible // indirect
	github.com/prometheus/client_golang v0.9.2 // indirect
	github.com/prometheus/common v0.0.0-20190104105734-b1c43a6df3ae // indirect
	github.com/prometheus/procfs v0.0.0-20190104112138-b1a0a9a36d74 // indirect
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/xanzy/go-gitlab v0.12.3
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1 // indirect
	golang.org/x/arch v0.0.0-20190312162104-788fe5ffcd8c // indirect
	golang.org/x/net v0.0.0-20190311183353-d8887717615a
	golang.org/x/oauth2 v0.0.0-20181203162652-d668ce993890 // indirect
	golang.org/x/time v0.0.0-20181108054448-85acf8d2951c // indirect
	golang.org/x/tools v0.0.0-20190608022120-eacb66d2a7c3 // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/webhooks.v3 v3.13.0
	gopkg.in/go-playground/webhooks.v5 v5.0.0-00010101000000-000000000000
	gopkg.in/inf.v0 v0.9.1 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
	k8s.io/api v0.0.0-20180904230853-4e7be11eab3f
	k8s.io/apiextensions-apiserver v0.0.0-20180910084140-b05d9bb7cc74 // indirect
	k8s.io/apimachinery v0.0.0-20180904193909-def12e63c512
	k8s.io/client-go v0.0.0-20180910083459-2cefa64ff137
	k8s.io/kube-openapi v0.0.0-20181114233023-0317810137be // indirect
	sigs.k8s.io/controller-runtime v0.0.0-20181113214900-0f0740db5681
	sigs.k8s.io/testing_frameworks v0.1.1 // indirect
)
