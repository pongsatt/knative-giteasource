/*
Copyright 2019 The TriggerMesh Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/pongsatt/knative-giteasource/pkg/ra"
	gitea "gopkg.in/go-playground/webhooks.v5/gitea"
)

const (
	// Environment variable containing the HTTP port
	envPort = "PORT"

	// Environment variable containing Gitea secret token
	envSecret = "GITEA_SECRET_TOKEN"
)

func main() {
	sink := flag.String("sink", "", "uri to send events to")

	flag.Parse()

	if sink == nil || *sink == "" {
		log.Fatalf("No sink given")
	}

	port := os.Getenv(envPort)
	if port == "" {
		port = "8080"
	}

	secretToken := os.Getenv(envSecret)
	if secretToken == "" {
		log.Fatalf("No secret token given")
	}

	log.Printf("Sink is: %q", *sink)

	ra := &ra.GiteaReceiveAdapter{
		Sink: *sink,
	}

	hook, err := gitea.New(gitea.Options.Secret(secretToken))

	if err != nil {
		log.Fatal(err)
	}

	addr := fmt.Sprintf(":%s", port)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		payload, err := hook.Parse(r,
			gitea.CreateEvent,
			gitea.DeleteEvent,
			gitea.ForkEvent,
			gitea.PushEvent,
			gitea.IssuesEvent,
			gitea.IssueCommentEvent,
			gitea.PullRequestEvent,
			gitea.ReleaseEvent)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), 500)
		}

		ra.HandleEvent(payload, r.Header)
	})
	http.ListenAndServe(addr, nil)
}
