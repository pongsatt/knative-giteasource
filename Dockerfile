# Build the manager binary
FROM golang:alpine as builder

ENV GO111MODULE=on

RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates

# Copy in the go src
WORKDIR /app
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY pkg/    pkg/
COPY cmd/    cmd/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o manager cmd/manager/*

# Copy the manager into a thin image
FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app/ .
ENTRYPOINT ["/manager"]
