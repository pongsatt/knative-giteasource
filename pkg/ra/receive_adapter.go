/*
Copyright 2019 The TriggerMesh Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package ra

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	models "code.gitea.io/gitea/modules/structs"
	"github.com/google/uuid"
	"github.com/knative/pkg/cloudevents"
)

const (
	giteaHeaderEvent = "Gitea-Event"
)

// GiteaReceiveAdapter converts incoming Gitea webhook events to
// CloudEvents and then sends them to the specified Sink
type GiteaReceiveAdapter struct {
	Sink   string
	Client *http.Client
}

// HandleEvent is invoked whenever an event comes in from Gitea
func (ra *GiteaReceiveAdapter) HandleEvent(payload interface{}, header http.Header) {
	err := ra.handleEvent(payload, header)
	if err != nil {
		log.Printf("unexpected error handling Getea event: %s", err)
	}
}

func (ra *GiteaReceiveAdapter) handleEvent(payload interface{}, hdr http.Header) error {

	giteaEventType := hdr.Get("X-" + giteaHeaderEvent)
	extensions := map[string]interface{}{
		cloudevents.HeaderExtensionsPrefix + giteaHeaderEvent: hdr.Get("X-" + giteaHeaderEvent),
	}

	log.Printf("Handling %s", giteaEventType)

	var eventID string
	if uuid, err := uuid.NewRandom(); err == nil {
		eventID = uuid.String()
	}

	cloudEventType := fmt.Sprintf("%s.%s", "dev.source.gitea", giteaEventType)
	source := sourceFromGiteaEvent(payload)

	return ra.postMessage(payload, source, cloudEventType, eventID, extensions)
}

func (ra *GiteaReceiveAdapter) postMessage(payload interface{}, source, eventType, eventID string,
	extensions map[string]interface{}) error {
	ctx := cloudevents.EventContext{
		CloudEventsVersion: cloudevents.CloudEventsVersion,
		EventType:          eventType,
		EventID:            eventID,
		EventTime:          time.Now(),
		Source:             source,
		Extensions:         extensions,
	}
	req, err := cloudevents.Binary.NewRequest(ra.Sink, payload, ctx)
	if err != nil {
		log.Printf("Failed to marshal the message: %+v : %s", payload, err)
		return err
	}

	log.Printf("Posting to %q", ra.Sink)
	client := ra.Client
	if client == nil {
		client = &http.Client{}
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		// TODO: in general, receive adapters may have to be able to retry for error cases.
		log.Printf("response Status: %s", resp.Status)
		body, _ := ioutil.ReadAll(resp.Body)
		log.Printf("response Body: %s", string(body))
	}
	return nil
}

func sourceFromGiteaEvent(payload interface{}) string {
	switch payload.(type) {
	case models.CreatePayload:
		return payload.(models.CreatePayload).Repo.HTMLURL
	case models.ReleasePayload:
		return payload.(models.ReleasePayload).Repository.HTMLURL
	case models.PushPayload:
		return payload.(models.PushPayload).Repo.HTMLURL
	case models.DeletePayload:
		return payload.(models.DeletePayload).Repo.HTMLURL
	case models.ForkPayload:
		return payload.(models.ForkPayload).Repo.HTMLURL
	case models.IssuePayload:
		return payload.(models.IssuePayload).Repository.HTMLURL
	case models.IssueCommentPayload:
		return payload.(models.IssueCommentPayload).Repository.HTMLURL
	case models.PullRequestPayload:
		return payload.(models.IssueCommentPayload).Repository.HTMLURL
	}
	return ""
}
