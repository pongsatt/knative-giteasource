/*
Copyright 2019 The TriggerMesh Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package giteasource

import (
	"fmt"
	"strconv"

	"code.gitea.io/gitea/modules/structs"
	"code.gitea.io/sdk/gitea"
)

type projectHookOptions struct {
	accessToken           string
	secretToken           string
	project               string
	id                    string
	url                   string
	owner                 string
	events                []string
	EnableSSLVerification bool
}

type projectHookClient interface {
	Create(options *projectHookOptions) (string, error)
	Delete(options *projectHookOptions, hookID string) error
}

type giteaHookClient struct{}

func (client giteaHookClient) Create(baseURL string, options *projectHookOptions) (string, error) {

	giteaClient := gitea.NewClient(baseURL, options.accessToken)

	if options.id != "" {
		hookID, err := strconv.Atoi(options.id)
		if err != nil {
			return "", fmt.Errorf("failed to convert hook id to int: " + err.Error())
		}
		hook, err := giteaClient.GetRepoHook(options.owner, options.project, int64(hookID))
		if err != nil {
			return "", fmt.Errorf("Failed to list project hooks for project:" + options.project + " due to" + err.Error())
		}

		return strconv.Itoa(int(hook.ID)), nil
	}

	hookOptions := structs.CreateHookOption{
		Active: true,
		Config: map[string]string{
			"content_type": "json",
			"url":          options.url,
			"secret":       options.secretToken,
		},
		Events: options.events,
		Type:   "gitea",
	}

	hook, err := giteaClient.CreateRepoHook(options.owner, options.project, hookOptions)
	if err != nil {
		return "", fmt.Errorf("Failed to add webhook to the project:" + options.project + " due to " + err.Error())
	}

	return strconv.Itoa(int(hook.ID)), nil
}

func (client giteaHookClient) Delete(baseURL string, options *projectHookOptions) error {
	if options.id != "" {
		hookID, err := strconv.Atoi(options.id)
		if err != nil {
			return fmt.Errorf("failed to convert hook id to int: " + err.Error())
		}
		giteaClient := gitea.NewClient(baseURL, options.accessToken)

		err = giteaClient.DeleteRepoHook(options.owner, options.project, int64(hookID))
		if err != nil {
			return fmt.Errorf("Failed to list project hooks for project: " + options.project)
		}
	}

	return nil
}
