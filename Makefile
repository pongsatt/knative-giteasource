# Image URL to use all building/pushing image targets
IMG ?= registry.gitlab.com/pongsatt/knative-giteasource/controller:latest
RA_IMG ?= registry.gitlab.com/pongsatt/knative-giteasource/receive-adapter:latest

all: test manager

# Run tests
test:
	go test ./cmd/... -coverprofile cover.out

# Build manager binary
manager:
	go build -o bin/manager cmd/manager/*

# Run against the configured Kubernetes cluster in ~/.kube/config
run:
	go run ./cmd/manager/main.go

# Install CRDs into a cluster
install:
	kubectl apply -f config/crds

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy:
	kubectl apply -f config/crds
	kustomize build config/default | kubectl apply -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests:
	go run sigs.k8s.io/controller-tools/cmd/controller-gen/main.go all

# Generate code
generate:
	go generate ./pkg/... ./cmd/...

# Build the docker image
docker-build: test
	docker build . -t ${IMG}
	@echo "updating kustomize image patch file for manager resource"
	sed -i'' -e 's@image: .*@image: '"${IMG}"'@' ./config/default/manager_image_patch.yaml

# Build the recieve adapater image
docker-build-ra: test
	docker build . -f Dockerfile.receive-adapter -t ${RA_IMG}

# Push the manager docker image
docker-push: docker-build
	docker push ${IMG}

docker-push-ra: docker-build-ra
	docker push ${RA_IMG}

kustomize:
	docker run --rm -i \
    -w /working/ \
    -v "$(PWD):/working" \
    traherom/kustomize-docker:1.0.5 \
    kustomize build config/default > release.yml